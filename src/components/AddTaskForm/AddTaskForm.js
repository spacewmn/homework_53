import React from 'react';

const AddTaskForm = (props) => {

    return (
            <form onSubmit={props.add} className="task-form">
                <input type="text" value={props.name} onChange={props.currentTask}/>
                <button type="submit" >Add</button>
            </form>
    )
};

export default AddTaskForm;