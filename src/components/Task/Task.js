import React from "react";

const Task = props => {
    return (
        <div className="task">
            <p>{props.name}
                <button type="button" onClick={props.remove}>-</button>
            </p>
        </div>
    );
};

export default Task;