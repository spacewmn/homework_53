import React, {Fragment, useState} from 'react';
import './App.css';
import Task from "./components/Task/Task";
import AddTaskForm from "./components/AddTaskForm/AddTaskForm";

const App = () => {
    const [tasks, setTasks] = useState([
        {name: 'Walk with a puppy', index: 1},
        {name: 'Call to Kate', index: 2},
    ]);

    const [newTasks, setNewTasks] = useState('')

    const currentTask = (event) => {
        setNewTasks(event.target.value);
    };

    const addTask = (event, inputtedText) => {
        event.preventDefault();
        if (inputtedText !== '') {
        const tasksCopy = [...tasks];
        const newTask = {
            index: Date.now(),
            name: newTasks
        }
        tasksCopy.push(newTask);
            setTasks(tasksCopy);
        }
    }

    const removeTask = id => {
        const index = tasks.findIndex(p => p.index === id)
        const tasksCopy = [...tasks];
        tasksCopy.splice(index, 1);
        setTasks(tasksCopy);
    };

    const taskDiv = (
        <div>
            {
                tasks.map((task) => {
                    return <Task
                        key={task.index}
                        name={task.name}
                        remove={() => removeTask(task.index)}>
                    </Task>
                })
            }
        </div>
    );

    return (
        <Fragment>
            <div className="App">
                <h1>To-Do List</h1>
            <AddTaskForm
                currentTask={currentTask}
                add={(event) => addTask(event, newTasks)}
            />
            {taskDiv}
            </div>
        </Fragment>
    )
}

export default App;
